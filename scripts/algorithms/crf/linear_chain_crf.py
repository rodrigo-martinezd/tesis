# -*- coding: utf-8 -*-
from __future__ import absolute_import

import sklearn_crfsuite
from sklearn_crfsuite import metrics
from scripts.utils import utils
from scripts.utils.features import sent2features, sent2labels


if __name__ == '__main__':
    import sys

    data_path = sys.argv[1]

    # Read args
    train_filename = sys.argv[2]
    test_filename = sys.argv[3]

    train_path = data_path + "/datasets/" + train_filename
    test_path = data_path + "/datasets/" + test_filename

    # Open files
    train_file = utils.read_file(train_path)
    test_file = utils.read_file(test_path)

    # Extract sentences
    train_sent = utils.get_words_and_labels(train_file)
    test_sent = utils.get_words_and_labels(test_file)

    # Dictionaries paths
    dictionaries_path = data_path + "/dictionaries/"
    gazetteers_path = data_path + "/gazetteers/"

    # Load stopwords set
    stopwords = utils.get_stopwords(data_path)

    # Load and build tries
    building_trie = utils.build_trie_from(dictionaries_path + "buildings_types.txt", stopwords=stopwords)
    neighborhood_trie = utils.build_trie_from(dictionaries_path + "neighborhood_types.txt", stopwords=stopwords)
    street_trie = utils.build_trie_from(dictionaries_path + "street_types.txt", stopwords=stopwords)
    zones_trie = utils.build_trie_from(dictionaries_path + "zones_types.txt", stopwords=stopwords)
    city_districts_trie = utils.build_trie_from(gazetteers_path + "city_districts.txt", stopwords=stopwords)
    provinces_trie = utils.build_trie_from(gazetteers_path + "provinces.txt", stopwords=stopwords)
    localities_trie = utils.build_trie_from(gazetteers_path + "localities.txt", stopwords=stopwords)
    regions_trie = utils.build_trie_from(gazetteers_path + "regions.txt", stopwords=stopwords)

    dictionaries = {
        "buildings": building_trie,
        "neighborhoods": neighborhood_trie,
        "streets": street_trie,
        "zones": zones_trie,
        "city_districts": city_districts_trie,
        "provinces": provinces_trie,
        "localities": localities_trie,
        "regions": regions_trie,
        "stopwords": stopwords,
    }

    # Configure dictionary lookup
    opts = dict()
    opts['enable_dictionary_lookup'] = True
    opts['dictionaries'] = dictionaries

    # Build Train data
    X_train = [sent2features(s, **opts) for s in train_sent]
    y_train = [sent2labels(s) for s in train_sent]

    # Build Test data
    X_test = [sent2features(s) for s in test_sent]
    y_test = [sent2labels(s) for s in test_sent]

    # Train
    crf = sklearn_crfsuite.CRF(
        algorithm='lbfgs',
        c1=0.1,
        c2=0.1,
        max_iterations=100,
        all_possible_transitions=True
    )

    crf.fit(X_train, y_train)

    # Test
    y_pred = crf.predict(X_test)

    labels = list(crf.classes_)
    labels.remove('O')
    f1_score = metrics.flat_f1_score(y_test, y_pred, average='weighted', labels=labels)

    print('f1 score: %f' % f1_score)

    # Inspect by classes

    # group B and I results
    sorted_labels = sorted(
        labels,
        key=lambda name: (name[1:], name[0])
    )
    print(metrics.flat_classification_report(
        y_test, y_pred, labels=sorted_labels, digits=3
    ))
