# -*- coding: utf-8 -*-
import re
from scripts.utils import utils
from sklearn_crfsuite import metrics
from scripts.utils.features import sent2labels, sent2features
from scripts.utils import trie
"""
    Rule Based algorithm

    Rules:

    1) if w is at begining and is one of (Av Avda Avenida, calle, psj pasaje, camino, etc...) then it's B_ST
    2) if w is the first number in the sentence and is preceeding by (#, N, Nro, Number, N°) then it's a B_SN
    3) Anything Between B_ST and B_SN excluding (#, N, Nro, Number, N°) it's B_S or I_S
    4) Anything Between B_SN and B_C is a prospect to one of this classes (U,UN, CTD, CT, N, Z, L, R, F)
    5) if w is "piso" and w_i+1 is a number (cardinal or ordinal) then all are a class F
    6) if w is one of (Villa, barrio, vecindad, vecindario) it's a B_N
    7) if w is "region" then everything that follows until the end or until find a country is a prospect to region
    8) if w is one type of building can be a Unit
    9) if w is one type of zones can be a zone
    10) if w is one of the city districts and city is already tagged then is a city district
    11) if w is a city but appears duplicated is a city only if there isn\'t a city district with that name
"""


def tag(sentences, **dictionaries):
    sentences_tagged = []
    for sentence in sentences:
        sentences_tagged.append(apply_rules(sentence, **dictionaries))
    return sentences_tagged


def apply_rules(sentence, **dictionaries):
    tagged = {}
    found_first_number = False
    street_number = None
    words = sentence.split()
    for i, w in enumerate(words):
        w_not_points = clean_word(w)
        # Street
        if i == 0 and trie.find_prefix(dictionaries['streets'], w_not_points)[0]:
            tagged.update({i:  (w, 'B_ST')})
        # country
        elif i == len(words) - 1 and re.search(r"(?iu)^(chile|cl)$", w_not_points):
            tagged.update({i: (w, 'B_C')})
        # Street number and Street
        elif w.isdigit() and not found_first_number:
            found_first_number = True
            street_number = w
            tagged.update({i: (w, 'B_SN')})

            # Tag every previous word between B_SN and B_ST from the begining as a street class
            for j in range(0, i):
                if not re.search(r"(?iu)^(#|nro|n|n°|number|num|numero)$", words[j].replace(".", "")):
                    if j == 0 and (not tagged[0] or tagged[0][1] != "B_ST"):
                        tagged.update({0: (words[0], "B_S")})
                    elif j == 1 and tagged[0] and tagged[0][1] == "B_ST":
                        tagged.update({1: (words[j], "B_S")})
                    elif j == 1 and tagged[0] and tagged[0][1] == "B_S":
                        tagged.update({j: (words[j], "I_S")})
                    else:
                        tagged.update({j: (words[j], "I_S")})
        # Street number
        elif w.isdigit() and found_first_number and w == street_number:
            tagged.update({i: (w, 'B_SN')})

        # Street number
        elif re.search(r"(?iu)^([a-z]{1,2}|[a-z]{1,2}-[a-z])$", w_not_points) and i > 0 and tagged[i-1] and tagged[i-1][1] == 'B_SN':
            tagged.update({i: (w, 'I_SN')})
        # Floor
        elif re.search(r"(?iu)piso", w_not_points):
            tagged.update({i: (w, 'B_F')})
        elif w.isdigit() or re.search(r"(?iu)([1-9]+|uno|dos|tres|cuatro|cinco|seis|siete|ocho|nueve|diez)", w_not_points) and \
                i > 0 and tagged[i-1][1] == 'B_F':
            tagged.update({i: (w, 'I_F')})
        # Units
        elif trie.find_prefix(dictionaries['buildings'], w_not_points)[0]:
            tagged.update({i: (w, 'B_U')})
        elif re.search(r"(?iu)^[a-z0-9-.]{1,3}$", w_not_points) and i > 0 and tagged[i-1] and tagged[i-1][1] == 'B_U':
            tagged.update({i: (w, 'B_UN')})
        elif re.search(r"(?iu)^[a-z0-9-.]{1,3}$", w_not_points) and i > 0 and tagged[i-1] and (tagged[i-1][1] == 'B_UN' or tagged[i-1][1] == 'I_UN'):
            tagged.update({i: (w, 'I_UN')})
        # Neighborhoods
        elif trie.find_prefix(dictionaries["neighborhoods"], w_not_points)[0]:
            if i > 0 and tagged[i-1] and tagged[i-1] == 'B_N':
                tagged.update({i: (w, 'I_N')})
            else:
                tagged.update({i: (w, 'B_N')})

        # City districts
        elif trie.find_prefix(dictionaries["city_districts"], w_not_points)[0]:
            if i > 0 and tagged[i-1] and tagged[i-1] == 'B_CTD':
                tagged.update({i: (w, 'I_CTD')})
            else:
                tagged.update({i: (w, 'B_CTD')})
        # Localities
        elif trie.find_prefix(dictionaries["localities"], w_not_points)[0]:
            if i > 0 and tagged[i-1] and tagged[i-1][1] == 'B_L':
                tagged.update({i: (w, 'I_L')})
            else:
                tagged.update({i: (w, 'B_L')})

        # provinces
        elif trie.find_prefix(dictionaries["provinces"], w_not_points)[0]:
            if i > 0 and tagged[i - 1] and tagged[i - 1] == 'B_PV':
                tagged.update({i: (w, 'I_PV')})
            else:
                tagged.update({i: (w, 'B_PV')})
        # Regions
        elif trie.find_prefix(dictionaries["regions"], w_not_points)[0]:
            if i > 0 and tagged[i-1] and tagged[i-1][1] == 'B_R':
                tagged.update({i: (w, 'I_R')})
            else:
                tagged.update({i: (w, 'B_R')})
        else:
            tagged.update({i: (w, 'O')})

    prunned_tagged = prune_tags(tagged, stopwords=dictionaries['stopwords'])

    if len(prunned_tagged) > len(sentence.split()):
        raise Exception("tagged sentence more extense than sentence")

    return build_sentence(prunned_tagged)


def prune_tags(tagged, stopwords=set()):
    return tagged


def build_sentence(tagged_words):
    return [item for key, item in tagged_words.items()]


def clean_word(word) -> str:
    return word.lower().replace(".", "")

if __name__ == '__main__':
    import sys
    data_path = sys.argv[1]
    test_path = sys.argv[2]

    test_file1 = utils.read_file(test_path)
    test_file2 = utils.read_file(test_path)
    test_words_and_labels = utils.get_words_and_labels(test_file1)
    test_sent = utils.get_sentences(test_file2)

    # Build test data
    y_test = [sent2labels(s) for s in test_words_and_labels]

    # Dictionaries paths
    dictionaries_path = data_path + "/dictionaries/"
    gazetteers_path = data_path + "/gazetteers/"

    # Load stopwords set
    stopwords = utils.get_stopwords(data_path)

    # Load and build tries
    building_trie = utils.build_trie_from(dictionaries_path + "buildings_types.txt", stopwords=stopwords)
    neighborhood_trie = utils.build_trie_from(dictionaries_path + "neighborhood_types.txt", stopwords=stopwords)
    street_trie = utils.build_trie_from(dictionaries_path + "street_types.txt", stopwords=stopwords)
    zones_trie = utils.build_trie_from(dictionaries_path + "zones_types.txt", stopwords=stopwords)
    city_districts_trie = utils.build_trie_from(gazetteers_path + "city_districts.txt", stopwords=stopwords)
    provinces_trie = utils.build_trie_from(gazetteers_path + "provinces.txt", stopwords=stopwords)
    localities_trie = utils.build_trie_from(gazetteers_path + "localities.txt", stopwords=stopwords)
    regions_trie = utils.build_trie_from(gazetteers_path + "regions.txt", stopwords=stopwords)

    dictionaries = {
        "buildings": building_trie,
        "neighborhoods": neighborhood_trie,
        "streets": street_trie,
        "zones": zones_trie,
        "city_districts": city_districts_trie,
        "provinces": provinces_trie,
        "localities": localities_trie,
        "regions": regions_trie,
        "stopwords": stopwords,
    }

    # Tag sentences with rule-based system
    y_pred = tag(test_sent, **dictionaries)
    y_pred_labels = [sent2labels(item) for item in y_pred]

    labels = [
        'B_S',
        'B_SN',
        'B_CTD',
        'B_CT',
        'B_C',
        'I_S',
        'I_CTD',
        'B_ST',
        'B_U',
        'B_UN',
        'B_N',
        'I_N',
        'B_PN',
        'I_PN',
        'I_CT',
        'I_SN',
        'B_F',
        'I_F',
        'B_R',
        'I_R',
        'I_UN',
        'B_RO',
        'I_RO',
        'B_RON',
        'B_L',
        'I_L',
        'B_Z',
        'I_Z',
        'I_RON',
        'I_U',
        'I_ST',
        'B_PVC',
        'I_PVC',
        'B_PV'
    ]

    f1_score = metrics.flat_f1_score(y_test, y_pred_labels, average='weighted', labels=labels)

    print('f1 score: %f' % f1_score)

    # Inspect by classes

    # group B and I results
    sorted_labels = sorted(
        labels,
        key=lambda name: (name[1:], name[0])
    )

    print(metrics.flat_classification_report(
        y_test, y_pred_labels, labels=sorted_labels, digits=3
    ))


