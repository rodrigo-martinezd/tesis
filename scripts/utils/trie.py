# -*- coding: utf-8 -*-

from __future__ import absolute_import
from typing import Tuple

"""
    In computer science, a trie, also called digital tree and sometimes radix tree or prefix tree,
    is a kind of search tree—an ordered tree data structure that is used to store a dynamic set or
    associative array where the keys are usually strings. Unlike a binary search tree, no node in
    the tree stores the key associated with that node; instead, its position in the tree defines the
    key with which it is associated. All the descendants of a node have a common prefix of the string
    associated with that node, and the root is associated with the empty string.

    code extracted from:
    https://towardsdatascience.com/implementing-a-trie-data-structure-in-python-in-less-than-100-lines-of-code-a877ea23c1a1

"""


class TrieNode(object):

    def __init__(self, char: str):
        self.char = char
        self.children = []

        # Is it the last character of the word.`
        self.word_finished = False

        # How many times this character appeared in the addition process
        self.counter = 1


def add(root, word: str):
    """
    Adding a word in the trie structure
    """
    node = root
    for char in word:
        found_in_child = False

        # Search for the character in the children of the present `node`
        for child in node.children:
            if child.char == char:

                # We found it, increase the counter by 1 to keep track that another
                # word has it as well
                child.counter += 1

                # And point the node to the child that contains this char
                node = child
                found_in_child = True
                break

        # We did not find it so add a new child
        if not found_in_child:
            new_node = TrieNode(char)
            node.children.append(new_node)

            # And then point node to the new child
            node = new_node

    # Everything finished. Mark it as the end of a word.
    node.word_finished = True


def find_prefix(root, prefix: str) -> Tuple[bool, int]:
    """
    Check and return
      1. If the prefix exsists in any of the words we added so far
      2. If yes then how may words actually have the prefix
    """
    node = root

    # If the root node has no children, then return False.
    # Because it means we are trying to search in an empty trie
    if not root.children:
        return False, 0

    for char in prefix:
        char_not_found = True

        # Search through all the children of the present `node`
        for child in node.children:
            if child.char == char:

                # We found the char existing in the child.
                char_not_found = False

                # Assign node as the child containing the char and break
                node = child
                break

        # Return False anyway when we did not find a char.
        if char_not_found:
            return False, 0

    return True, node.counter
