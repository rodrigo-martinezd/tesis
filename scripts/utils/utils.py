# -*- coding: utf-8 -*-
from __future__ import absolute_import
import re
import codecs
from typing import List
from scripts.utils import trie

allowed_dictionaries = [
    'buildings_types',
    'neighborhood_types',
    'stopwords',
    'street_types',
    'zones_types',
]

allowed_gazetteers = [
    'city_districts',
    'localities',
    'provinces',
    'regions'
]


def read_file(path: str):
    # return utf-8 file pointer
    return codecs.open(path, "r", "utf-8")


def get_sentences(file) -> List:
    sentences = []
    for line in file.readlines():
        line = line.replace(r"\n", "")
        sentence = []
        words_and_labels = line.split()
        if len(words_and_labels) > 0:
            for word_and_label in words_and_labels:
                words = word_and_label.split("|")
                if len(words) == 2 and words[0]:
                    sentence.append(words[0])

        if len(sentence) > 0:
            sentences.append(" ".join(sentence))

    return sentences


def get_words_and_labels(file) -> List:
    # return matrix of tuples where  is (token, label) and every row is a sentence
    sentences = []
    for line in file.readlines():
        line = line.replace(r"\n", "")
        sentence = []
        words_and_labels = line.split()
        if len(words_and_labels) > 0:
            for word_and_label in words_and_labels:
                words = word_and_label.split("|")
                if len(words) == 2 and words[0] and words[1]:
                    sentence.append((words[0], words[1]))

        if len(sentence) > 0:
            sentences.append(sentence)

    return sentences


def get_stopwords(data_path) -> set:
    src_path = data_path + "/dictionaries/stopwords.txt"
    file_instance = read_file(src_path)
    bag_of_words = [word.replace("\n", "") for word in file_instance.readlines()]
    return frozenset(bag_of_words)


def build_trie_from(src_path, stopwords=frozenset()):
    match = re.search(r"(?<=/)(?P<filename>[a-z_]+)\.txt$", src_path)
    filename = match.group('filename') if match else ""

    if filename not in allowed_dictionaries and filename not in allowed_gazetteers:
        raise Exception("can\'t open an unregistered dictionary or gazetteer")

    file_instance = read_file(src_path)
    bag_of_words = list()
    for line in file_instance.readlines():
        line = line.replace("\n", "")
        segments = line.split("|")
        for segment in segments:
            if re.search(r"\s", segment):
                bag_of_words += segment.split()
            else:
                bag_of_words.append(segment)

    filtered_words = filter(lambda word: word not in stopwords, bag_of_words)
    unique_words = frozenset(filtered_words)

    # Build trie instance using the unique words
    root = trie.TrieNode('*')
    for word in unique_words:
        trie.add(root, word)

    return root


if __name__ == '__main__':
    import sys
    path = sys.argv[1]
    print(path)
    file = read_file(path)
    print(get_words_and_labels(file))
