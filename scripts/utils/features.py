# -*- coding: utf-8 -*-
from scripts.utils import trie


def clean_word(word):
    return word.lower().replace(".", "")


def word2features(sent, i, **opts):
    word = sent[i][0]
    features = dict()

    word_level_features = {
        'word.istitle()': word.istitle(),
        'word.isupper()': word.isupper(),
        'word.islower()': word.islower(),
        'w[0].lower()': word.lower(),
        'w[0].upper()': word.upper(),
        #'word.isMixed': 0,
        'word.isalpha()': word.isalpha(),
        'word.isalnum()': word.isalnum(),
        'word.isnumeric()': word.isnumeric(),
        #'word.haveInnerPoint': 0,
        #'word.haveContraction':0,
        #'word.containDash':0,
        #'word.haveSpecialChar':0,
        #'word.haveEndPoint':0,
        #'word.isComma':0,
        'word.length': len(word),
        'w[0]': word,
    }

    if i == 0:
        word_level_features['BOS'] = True
    elif i == len(sent) - 1:
        word_level_features['EOS'] = True

    morphological_features = {
        #'word.shape':0,
        #'word.summarizedShape': 0,
        #'w[0]|w[1]|w[2]':0,
        #'w[0]|w[1]': 0,
    }

    contextual_features = {
        'sentence.length': len(sent),
        'word.position': i
    }

    if i < len(sent) - 2 and i - 2 > 0:
        contextual_features.update({
            'w[-2]': sent[i - 2][0],
            'w[-1]': sent[i - 1][0],
            'w[1]': sent[i + 1][0],
            'w[2]': sent[i + 2][0],
        })
    elif i < len(sent) - 1 and i - 1 > 0:
        contextual_features.update({
            'w[-1]': sent[i - 1][0],
            'w[1]': sent[i + 1][0],
        })

    if opts and opts['enable_dictionary_lookup']:
        dictionaries = opts['dictionaries']
        gazetteers_features = {
            'is_city_district': trie.find_prefix(dictionaries['city_districts'], clean_word(word))[0],
            'is_region': trie.find_prefix(dictionaries['regions'], clean_word(word))[0],
            'is_locality': trie.find_prefix(dictionaries['localities'], clean_word(word))[0],
            'is_province': trie.find_prefix(dictionaries['provinces'], clean_word(word))[0],
        }

        features.update(gazetteers_features)

    features.update(word_level_features)

    #features.update(morphological_features)
    features.update(contextual_features)

    return features


def sent2features(sent, **opts):
    return [word2features(sent, i, **opts) for i in range(len(sent))]


def sent2labels(sent):
    return [label for token, label in sent]


def sent2tokens(sent):
    return [token for token, label in sent]
