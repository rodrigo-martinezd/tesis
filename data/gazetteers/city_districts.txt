aisen|aysen
algarrobo
alhue|alue
alto biobio
alto del carmen|alto carmen
alto hospicio
ancud
andacollo|andacolo
angol
antartica
antofagasta|antofa
antuco
arauco
arica
buin
bulnes|bulne
cabildo
cabo de hornos|cabo hornos|cabo de horno
cabrero
calama
calbuco|calvuco
caldera
calera
calera de tango|calera tango|caleta tango
calle larga
camarones
camina
canela
canete
carahue
cartagena
casablanca
castro
catemu
cauquenes|cauqenes|cauqen|caukenes
cerrillos|cerrillo|los cerrillos
cerro navia
chaiten
chanco
chanaral
chepica
chiguayante
chile chico
chillan
chillan viejo
chimbarongo
cholchol
chonchi
cisnes|cisne
cobquecura|coquecura|coqecura|covquecura
cochamo
cochrane
codegua|codega
coelemu|colemu
coihueco|coigueco
coinco
colbun
colchane
colina
collipulli|colipulli|collipuli
coltauco
combarbala
concepcion|conce|la concepcion de maria purisima del nuevo extremo
conchali
concon
constitucion|contitucion
contulmo
copiapo
coquimbo
coronel
corral
coyhaique|collaique|coyaique|coyhaiqe
cunco
curacautin
curacavi
curaco de velez|curaco velez
curanilahue|curanilaue
curarrehue|curarreue
curepto
curico
dalcahue|dalcaue
diego de almagro|diego almagro
donihue|doniue
el bosque|bosque
el carmen|carmen
el monte|monte
el quisco|quisco
el tabo|tabo
empedrado
ercilla
estacion central|est central
florida
freire
freirina
fresia
frutillar
futaleufu
futrono
galvarino
general lagos
gorbea
graneros
guaitecas
hijuelas
hualaihue|hualaiue
hualane
hualpen
hualqui
huara
huasco
huechuraba
illapel
independencia|indepen|inde|indep|idep
iquique
isla de maipo|isla maipo
isla de pascua|isla pascua
juan fernandez
la cisterna|cisterna
la cruz|cruz
la estrella|estrella
la florida|florida
la granja|granja
la higuera|higuera
la ligua|ligua
la pintana|pintana
la reina|reina
la serena|serena
la union|union
lago ranco
lago verde
laguna blanca
laja
lampa
lanco
las cabras
las condes
lautaro
lebu
licanten
limache
linares
litueche
llanquihue
llay-llay|llay llay|yai yai|yay yay
lo barnechea|barnechea
lo espejo|espejo
lo prado
lolol
loncoche
longavi
lonquimay
los alamos|lo alamos
los andes|lo andes
los angeles|lo angeles
los lagos|lo lagos
los muermos|lo muermos
los sauces|lo sauces
los vilos|lo vilos|lo viloh
lota
lumaco
machali
macul
mafil
maipu
malloa
marchigue
maria elena
maria pinto
mariquina
maule
maullin
mejillones
melipeuco
melipilla
molina
monte patria
mostazal
mulchen
nacimiento
nancagua
natales
navidad
negrete
ninhue
nogales
nueva imperial
niquen
nunoa
o'higgins
olivar
ollague
olmue
osorno
ovalle
padre hurtado
padre las casas|padre casas
paihuano
paillaco
paine
palena
palmilla
panguipulli
panquehue
papudo
paredones
parral
pedro aguirre cerda|pdro aguirre cerda|pac|pdro aguirre|presidente pedro aguirre cerda|p aguirre cerda|p aguirre c|pedro a c|pedro agire serda
pelarco
pelluhue|pelluue
pemuco
pencahue|pencaue
penco
penaflor
penalolen
peralillo
perquenco
petorca
peumo
pica
pichidegua
pichilemu
pinto
pirque
pitrufquen
placilla
portezuelo
porvenir
pozo almonte
primavera
providencia
puchuncavi
pucon
pudahuel
puente alto
puerto montt|puerto mont
puerto octay|puerto octai
puerto varas
pumanque
punitaqui
punta arenas
puqueldon
puren
purranque
putaendo
putre
puyehue|puyeue
queilen
quellon
quemchi
quilaco
quilicura
quilleco
quillon
quillota
quilpue
quinchao
quinta de tilcoco|quinta tilcoco
quinta normal|qta normal
quintero
quirihue
rancagua
ranquil
rauco
recoleta
renaico
renca
rengo
requinoa
retiro
rinconada
rio bueno
rio claro
rio hurtado
rio ibanez
rio negro
rio verde
romeral
saavedra
sagrada familia
salamanca
san antonio
san bernardo|sn bdo|san bdo|sn bernardo
san carlos
san clemente
san esteban
san fabian
san felipe
san fernando|sn fdo|san fdo|sn fernando
san gregorio
san ignacio
san javier
san joaquin
san jose de maipo|sn jose maipo|sn jose de maipo|san jose maipo
san juan de la costa|sn juan de la costa|san juan costa
san miguel
san nicolas
san pablo
san pedro
san pedro de atacama|san pdro de atacama|sn pedro de atacama|san pedro atacama
san pedro de la paz|san pdro de la pz|san pdro de la paz|sn pdro de la paz
san rafael
san ramon
san rosendo
san vicente
santa barbara|st barbara|sta cruz
santa cruz|st cruz|sta cruz
santa juana
santa maria|sta maria
santiago|stgo|santiago de chile|stgo de chile
santo domingo
sierra gorda
talagante
talca
talcahuano|talcauano
taltal
temuco|tco
teno
teodoro schmidt|teodoro sshmit|teodoro schmit
tierra amarilla
tiltil
timaukel|timauqel
tirua
tocopilla
tolten
tome
torres del paine
tortel
traiguen
treguaco
tucapel
valdivia
vallenar
valparaiso
vichuquen
victoria
vicuna
vilcun
villa alegre
villa alemana
villarrica
vina del mar
vitacura
yerbas buenas
yumbel
yungay
zapallar
